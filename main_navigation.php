<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <!-- Main -->
            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i>
            </li>
            <li class="active"><a href="index.html"><i class="icon-home4"></i>
                    <span>Dashboard</span></a></li>
            <li>
                <a href="#"><i class="icon-droplet2"></i> <span>Color system</span></a>
                <ul>
                    <li><a href="colors_primary.html">Primary palette</a></li>
                    <li><a href="colors_danger.html">Danger palette</a></li>
                    <li><a href="colors_success.html">Success palette</a></li>
                    <li class="navigation-divider"></li>
                    <li><a href="colors_pink.html">Pink palette</a></li>
                    <li><a href="colors_violet.html">Violet palette</a></li>
                    <li><a href="colors_grey.html">Grey palette</a></li>
                    <li><a href="colors_slate.html">Slate palette</a></li>
                </ul>
            </li>
            <li><a href="changelog.html"><i class="icon-list-unordered"></i> <span>Changelog <span
                            class="label bg-blue-400">1.4</span></span></a></li>
            <li><a href="../../RTL/index.html"><i class="icon-width"></i> <span>RTL version</span></a>
            </li>
            <!-- /main -->

        </ul>
    </div>
</div>