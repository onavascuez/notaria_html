<?php include "head.php"; ?>

<?php include "main_navbar.php"; ?>

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <?php include "sidebar.php"; ?>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <?php include "page_header.php"; ?>

                <?php include "page_breadcrumb.php"; ?>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">


                <!-- Dashboard content -->
                <div class="row">
                    <div class="col-lg-8">

                        SEC 1

                    </div>

                    <div class="col-lg-4">

                        SEC 2

                    </div>
                </div>
                <!-- /dashboard content -->

                <!-- Footer -->
                <?php include "footer.php"; ?>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
</body>
</html>